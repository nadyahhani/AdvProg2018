package id.ac.ui.cs.advprog.tutorial1.strategy;

/**
 * Created by nadyahhani on 20/02/18.
 */
public class Swim implements SwimBehavior{

    @Override
    public void swim() {
        System.out.println("Splash");
    }
}
