package id.ac.ui.cs.advprog.tutorial1.strategy;

/**
 * Created by nadyahhani on 20/02/18.
 */
public interface SwimBehavior {
    void swim();
}
