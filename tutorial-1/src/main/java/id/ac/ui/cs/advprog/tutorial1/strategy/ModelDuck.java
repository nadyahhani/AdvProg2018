package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    @Override
    public void display() {
        System.out.println("I'm a model duck");
    }
}
