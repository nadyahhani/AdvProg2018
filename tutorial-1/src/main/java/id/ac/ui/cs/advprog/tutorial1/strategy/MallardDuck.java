package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    @Override
    public void display() {
        System.out.println("I'm a mallard duck");
    }
}
